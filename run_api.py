# Test Server.
from os import environ

from api import create_api

if environ.get('FLASK_ENV') is None:
    print('FLASK_ENV not set')
app = create_api(environ.get('FLASK_ENV') or 'development')
