#!/bin/sh

source venv/bin/activate 
ls -l /home/user/venv/bin/gunicorn
echo $USER
/home/user/venv/bin/gunicorn -b :5000 --access-logfile - --error-logfile - run_api:app
