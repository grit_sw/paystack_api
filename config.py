import os

class Config(object):
	DEBUG = False
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SECRET_KEY = os.urandom(12)
	PAYSTACK_SECRET = os.environ['PAYSTACK_SECRET']
	# SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
	NEW_PIN_URL = os.environ['NEW_PIN_URL']
	CREDIT_URL = os.environ['CREDIT_URL']

	@staticmethod
	def init_app(app):
		pass


class Development(Config):
	DEBUG = True
	# SQLALCHEMY_DATABASE_URI = 'postgresql://dev:postgres@localhost/payments'

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)

	@staticmethod
	def init_app(app, *args):
		pass


class Testing(Config):
	TESTING = True
	# SQLALCHEMY_DATABASE_URI = 'sqlite://'


class Staging(Config):
	NEW_PIN_URL = os.environ['NEW_PIN_URL']

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


class Production(Config):
	NEW_PIN_URL = os.environ['NEW_PIN_URL']

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


config = {
	'development': Development,
	'testing': Testing,
	'Staging': Staging,
	'Production': Production,

	'default': Development,
}
