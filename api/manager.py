from threading import Thread
from pprint import pprint

import requests

from api import app
from logger import logger


def save_transaction(credit_url, new_pin_url, account_data, keypad_user, pin_data):
	response = {}
	logger.info("Crediting Account")
	try:
		account_response = requests.post(credit_url, data=account_data)
	except requests.ConnectionError as e:
		logger.critical(e)
		response['success'] = False
		return response, 400

	if account_response.status_code not in {200, 201}:
		response['success'] = False
		return response, 400
	logger.info(account_response.json())

	if keypad_user:
		logger.info("Generating Pin")
		try:
			pin_response = requests.post(new_pin_url, pin_data)
		except requests.ConnectionError as e:
			logger.critical(e)
			response['success'] = False
			return response, 400

		if pin_response.status_code != 201:
			response['success'] = False
			return response, 400
		logger.info("Pin Generation Successful")



class PaystackManager(object):

	def new_payment(self, payload):
		response = {}
		event = payload['event']
		if event == "charge.success":
			pprint(payload)
			amount_paid = float(payload['data']['amount'])
			paid_at = payload['data']['paid_at']
			reference = payload['data']['reference']

			brand = payload['data']['authorization']['brand']
			last_four = payload['data']['authorization']['last4']
			paystack_auth = payload['data']['authorization']['authorization_code']
			if not payload['data']['metadata']:
				logger.info('\n\n\n\n\n')
				logger.info('Invalid message format')
				logger.info('\n\n\n\n\n')
				response['success'] = False
				response['message'] = 'Invalid metadata sent'
				return response, 200  # invalid message format. Send a success message so paystack doesn't keep resending it.

			customer_id = payload['data']['metadata']['user_id']
			customer_mobile = payload['data']['metadata']['phone_number']
			customer_email = payload['data']['metadata']['email']
			customer_full_name = payload['data']['metadata']['full_name']
			group_id = payload['data']['metadata']['group_id']
			tx_performed_by = payload['data']['metadata']['tx_performed_by']
			keypad_user = payload['data']['metadata']['keypad_user']
			meter = payload['data']['metadata'].get('meter', {})
			energy_units_purchased = payload['data']['metadata'].get('energy_units_purchased')

			meter_id = meter.get("device_id") if meter else None

			account_data = {
				'paid_at': paid_at,
				'group_id': group_id,
				'reference': reference,
				'customer_id': customer_id,
				'tx_performed_by': tx_performed_by,
				'is_working_balance': not(keypad_user),
				'paystack_auth': paystack_auth,
				'last_four': last_four,
				'brand': brand,
				'meter_id': meter_id,
				'amount_paid': amount_paid,
				'energy_units_purchased': energy_units_purchased,
			}

			pin_data = {
				'reference': reference,
				'group_id': group_id,
				'mobile': customer_mobile,
				'email': customer_email,
				'full_name': customer_full_name,
				'meter_id': meter_id,
				'amount_paid': amount_paid,
				'energy_units_purchased': energy_units_purchased,
			}


			credit_url = app.config['CREDIT_URL']
			new_pin_url = app.config['NEW_PIN_URL']
			t = Thread(target=save_transaction, args=(credit_url, new_pin_url, account_data, keypad_user, pin_data,))
			t.start()

			response['success'] = True
			return response, 200

		response['success'] = False
		return response, 400
