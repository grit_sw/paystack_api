import hashlib
import hmac
from pprint import pprint

from flask import current_app, request
from flask_restplus import Namespace, Resource

from api import api
from api.manager import PaystackManager
from logger import logger

payments = Namespace(
	'payment', description='Api for managing payments for prosumers')

def check_headers(paystack_header):
	hex_digest = hmac.new(str.encode(current_app.config.get('PAYSTACK_SECRET')), request.get_data(), hashlib.sha512).hexdigest()
	is_equal = hmac.compare_digest(paystack_header, hex_digest)
	return is_equal


@payments.route('/new-payment/')
class Paystack(Resource):

	def post(self):
		paystack_header = request.headers.get('X-Paystack-Signature')
		data = request.values.to_dict()
		payload = api.payload or data
		manager = PaystackManager()
		logger.info("\n\n\n\n")
		logger.info("New Paystack Payment")
		valid_tx = check_headers(paystack_header)
		if not valid_tx:
			logger.info("INVALID PAYMENT SIGNATURE")
			# logger.info(payload)
			pprint(payload)
			return {}, 400
		response, status_code = manager.new_payment(payload)
		return response, status_code

