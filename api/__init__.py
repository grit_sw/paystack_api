from flask import Flask
from flask_restplus import Api

from config import config
from logger import logger
api = Api()


app = Flask(__name__)
def create_api(config_name):
	try:
		init_config = config[config_name]()
	except KeyError as e:
		logger.critical(e)
		raise
	except Exception as e:
		logger.critical(e)
		# For unforseen exceptions
		raise

	print('Running in {} Mode'.format(init_config))
	config_object = config.get(config_name)

	app.config.from_object(config_object)

	from .controllers import payments as ns1
	api.add_namespace(ns1, path='/paystack')

	api.init_app(app)

	return app
